[require]
GLSL >= 4.30

[vertex shader]
#version 430
#extension GL_ARB_texture_gather: enable
#ifdef GL_ARB_texture_gather
#  define GPU_ARB_texture_gather
#endif
#extension GL_ARB_shader_draw_parameters : enable
#define GPU_ARB_shader_draw_parameters
#define gpu_BaseInstance gl_BaseInstanceARB
#extension GL_ARB_gpu_shader5 : enable
#define GPU_ARB_gpu_shader5
#extension GL_ARB_texture_cube_map_array : enable
#define GPU_ARB_texture_cube_map_array
#extension GL_ARB_conservative_depth : enable
#extension GL_ARB_shader_image_load_store: enable
#extension GL_ARB_shading_language_420pack: enable
#extension GL_AMD_vertex_shader_layer: enable
#define gpu_Layer gl_Layer
#define gpu_InstanceIndex (gl_InstanceID + gpu_BaseInstance)
#define gpu_Array(_type) _type[]
#define DFDX_SIGN 1.0
#define DFDY_SIGN 1.0

/* Texture format tokens -- Type explictness required by other Graphics APIs. */
#define depth2D sampler2D
#define depth2DArray sampler2DArray
#define depth2DMS sampler2DMS
#define depth2DMSArray sampler2DMSArray
#define depthCube samplerCube
#define depthCubeArray samplerCubeArray
#define depth2DArrayShadow sampler2DArrayShadow

/* Backend Functions. */
#define select(A, B, mask) mix(A, B, mask)

bool is_zero(vec2 A)
{
  return all(equal(A, vec2(0.0)));
}

bool is_zero(vec3 A)
{
  return all(equal(A, vec3(0.0)));
}

bool is_zero(vec4 A)
{
  return all(equal(A, vec4(0.0)));
}
#define GPU_SHADER
#define GPU_INTEL
#define OS_UNIX
#define GPU_OPENGL
#define GPU_VERTEX_SHADER
#define USE_GPU_SHADER_CREATE_INFO

/* Pass Resources. */
layout(binding = 0) uniform sampler2D image;

/* Batch Resources. */

/* Push Constants. */
uniform mat4 ModelViewProjectionMatrix;
uniform vec4 color;
uniform vec4 rect_icon;
uniform vec4 rect_geom;


/* Inputs. */

/* Interfaces. */
out smooth_tex_coord_interp_iface{
  smooth vec2 texCoord_interp;
};

/**
 * Simple shader that just draw one icon at the specified location
 * does not need any vertex input (producing less call to immBegin/End)
 */

void main()
{
  vec2 uv;
  vec2 co;

  if (gl_VertexID == 0) {
    co = rect_geom.xw;
    uv = rect_icon.xw;
  }
  else if (gl_VertexID == 1) {
    co = rect_geom.xy;
    uv = rect_icon.xy;
  }
  else if (gl_VertexID == 2) {
    co = rect_geom.zw;
    uv = rect_icon.zw;
  }
  else {
    co = rect_geom.zy;
    uv = rect_icon.zy;
  }

  gl_Position = ModelViewProjectionMatrix * vec4(co, 0.0f, 1.0f);
  texCoord_interp = uv;
}

[fragment shader]
#version 430
#extension GL_ARB_texture_gather: enable
#ifdef GL_ARB_texture_gather
#  define GPU_ARB_texture_gather
#endif
#extension GL_ARB_shader_draw_parameters : enable
#define GPU_ARB_shader_draw_parameters
#define gpu_BaseInstance gl_BaseInstanceARB
#extension GL_ARB_gpu_shader5 : enable
#define GPU_ARB_gpu_shader5
#extension GL_ARB_texture_cube_map_array : enable
#define GPU_ARB_texture_cube_map_array
#extension GL_ARB_conservative_depth : enable
#extension GL_ARB_shader_image_load_store: enable
#extension GL_ARB_shading_language_420pack: enable
#extension GL_AMD_vertex_shader_layer: enable
#define gpu_Layer gl_Layer
#define gpu_InstanceIndex (gl_InstanceID + gpu_BaseInstance)
#define gpu_Array(_type) _type[]
#define DFDX_SIGN 1.0
#define DFDY_SIGN 1.0

/* Texture format tokens -- Type explictness required by other Graphics APIs. */
#define depth2D sampler2D
#define depth2DArray sampler2DArray
#define depth2DMS sampler2DMS
#define depth2DMSArray sampler2DMSArray
#define depthCube samplerCube
#define depthCubeArray samplerCubeArray
#define depth2DArrayShadow sampler2DArrayShadow

/* Backend Functions. */
#define select(A, B, mask) mix(A, B, mask)

bool is_zero(vec2 A)
{
  return all(equal(A, vec2(0.0)));
}

bool is_zero(vec3 A)
{
  return all(equal(A, vec3(0.0)));
}

bool is_zero(vec4 A)
{
  return all(equal(A, vec4(0.0)));
}
#define GPU_SHADER
#define GPU_INTEL
#define OS_UNIX
#define GPU_OPENGL
#define GPU_FRAGMENT_SHADER
#define USE_GPU_SHADER_CREATE_INFO

/* Pass Resources. */
layout(binding = 0) uniform sampler2D image;

/* Batch Resources. */

/* Push Constants. */
uniform mat4 ModelViewProjectionMatrix;
uniform vec4 color;
uniform vec4 rect_icon;
uniform vec4 rect_geom;


/* Interfaces. */
in smooth_tex_coord_interp_iface{
  smooth vec2 texCoord_interp;
};
layout(depth_any) out float gl_FragDepth;

/* Outputs. */
layout(location = 0) out vec4 fragColor;


void main()
{
  fragColor = texture(image, texCoord_interp) * color;
}

