[require]
GLSL >= 4.30

[vertex shader]
#version 430
#extension GL_ARB_texture_gather: enable
#ifdef GL_ARB_texture_gather
#  define GPU_ARB_texture_gather
#endif
#extension GL_ARB_shader_draw_parameters : enable
#define GPU_ARB_shader_draw_parameters
#define gpu_BaseInstance gl_BaseInstanceARB
#extension GL_ARB_gpu_shader5 : enable
#define GPU_ARB_gpu_shader5
#extension GL_ARB_texture_cube_map_array : enable
#define GPU_ARB_texture_cube_map_array
#extension GL_ARB_conservative_depth : enable
#extension GL_ARB_shader_image_load_store: enable
#extension GL_ARB_shading_language_420pack: enable
#extension GL_AMD_vertex_shader_layer: enable
#define gpu_Layer gl_Layer
#define gpu_InstanceIndex (gl_InstanceID + gpu_BaseInstance)
#define gpu_Array(_type) _type[]
#define DFDX_SIGN 1.0
#define DFDY_SIGN 1.0

/* Texture format tokens -- Type explictness required by other Graphics APIs. */
#define depth2D sampler2D
#define depth2DArray sampler2DArray
#define depth2DMS sampler2DMS
#define depth2DMSArray sampler2DMSArray
#define depthCube samplerCube
#define depthCubeArray samplerCubeArray
#define depth2DArrayShadow sampler2DArrayShadow

/* Backend Functions. */
#define select(A, B, mask) mix(A, B, mask)

bool is_zero(vec2 A)
{
  return all(equal(A, vec2(0.0)));
}

bool is_zero(vec3 A)
{
  return all(equal(A, vec3(0.0)));
}

bool is_zero(vec4 A)
{
  return all(equal(A, vec4(0.0)));
}
#define GPU_SHADER
#define GPU_INTEL
#define OS_UNIX
#define GPU_OPENGL
#define GPU_VERTEX_SHADER
#define USE_GPU_SHADER_CREATE_INFO

/* Pass Resources. */

/* Batch Resources. */

/* Push Constants. */
uniform mat4 ModelViewProjectionMatrix;
uniform vec4 parameters[4];
uniform float alpha;


/* Inputs. */
layout(location = 0) in uint vflag;

/* Interfaces. */
out gpu_widget_shadow_iface{
  smooth float shadowFalloff;
};

#define BIT_RANGE(x) uint((1 << x) - 1)

/* 2 bits for corner */
/* Attention! Not the same order as in UI_interface.h!
 * Ordered by drawing order. */
#define BOTTOM_LEFT 0u
#define BOTTOM_RIGHT 1u
#define TOP_RIGHT 2u
#define TOP_LEFT 3u
#define CNR_FLAG_RANGE BIT_RANGE(2)

/* 4bits for corner id */
#define CORNER_VEC_OFS 2u
#define CORNER_VEC_RANGE BIT_RANGE(4)
const vec2 cornervec[36] = vec2[36](vec2(0.0, 1.0),
                                    vec2(0.02, 0.805),
                                    vec2(0.067, 0.617),
                                    vec2(0.169, 0.45),
                                    vec2(0.293, 0.293),
                                    vec2(0.45, 0.169),
                                    vec2(0.617, 0.076),
                                    vec2(0.805, 0.02),
                                    vec2(1.0, 0.0),
                                    vec2(-1.0, 0.0),
                                    vec2(-0.805, 0.02),
                                    vec2(-0.617, 0.067),
                                    vec2(-0.45, 0.169),
                                    vec2(-0.293, 0.293),
                                    vec2(-0.169, 0.45),
                                    vec2(-0.076, 0.617),
                                    vec2(-0.02, 0.805),
                                    vec2(0.0, 1.0),
                                    vec2(0.0, -1.0),
                                    vec2(-0.02, -0.805),
                                    vec2(-0.067, -0.617),
                                    vec2(-0.169, -0.45),
                                    vec2(-0.293, -0.293),
                                    vec2(-0.45, -0.169),
                                    vec2(-0.617, -0.076),
                                    vec2(-0.805, -0.02),
                                    vec2(-1.0, 0.0),
                                    vec2(1.0, 0.0),
                                    vec2(0.805, -0.02),
                                    vec2(0.617, -0.067),
                                    vec2(0.45, -0.169),
                                    vec2(0.293, -0.293),
                                    vec2(0.169, -0.45),
                                    vec2(0.076, -0.617),
                                    vec2(0.02, -0.805),
                                    vec2(0.0, -1.0));

#define INNER_FLAG uint(1 << 10) /* is inner vert */

/* radi and rad per corner */
#define recti parameters[0]
#define rect parameters[1]
#define radsi parameters[2].x
#define rads parameters[2].y
#define roundCorners parameters[3]

void main()
{
  uint cflag = vflag & CNR_FLAG_RANGE;
  uint vofs = (vflag >> CORNER_VEC_OFS) & CORNER_VEC_RANGE;

  vec2 v = cornervec[cflag * 9u + vofs];

  bool is_inner = (vflag & INNER_FLAG) != 0u;

  shadowFalloff = (is_inner) ? 1.0 : 0.0;

  /* Scale by corner radius */
  v *= roundCorners[cflag] * ((is_inner) ? radsi : rads);

  /* Position to corner */
  vec4 rct = (is_inner) ? recti : rect;
  if (cflag == BOTTOM_LEFT) {
    v += rct.xz;
  }
  else if (cflag == BOTTOM_RIGHT) {
    v += rct.yz;
  }
  else if (cflag == TOP_RIGHT) {
    v += rct.yw;
  }
  else /* (cflag == TOP_LEFT) */ {
    v += rct.xw;
  }

  gl_Position = ModelViewProjectionMatrix * vec4(v, 0.0, 1.0);
}

[fragment shader]
#version 430
#extension GL_ARB_texture_gather: enable
#ifdef GL_ARB_texture_gather
#  define GPU_ARB_texture_gather
#endif
#extension GL_ARB_shader_draw_parameters : enable
#define GPU_ARB_shader_draw_parameters
#define gpu_BaseInstance gl_BaseInstanceARB
#extension GL_ARB_gpu_shader5 : enable
#define GPU_ARB_gpu_shader5
#extension GL_ARB_texture_cube_map_array : enable
#define GPU_ARB_texture_cube_map_array
#extension GL_ARB_conservative_depth : enable
#extension GL_ARB_shader_image_load_store: enable
#extension GL_ARB_shading_language_420pack: enable
#extension GL_AMD_vertex_shader_layer: enable
#define gpu_Layer gl_Layer
#define gpu_InstanceIndex (gl_InstanceID + gpu_BaseInstance)
#define gpu_Array(_type) _type[]
#define DFDX_SIGN 1.0
#define DFDY_SIGN 1.0

/* Texture format tokens -- Type explictness required by other Graphics APIs. */
#define depth2D sampler2D
#define depth2DArray sampler2DArray
#define depth2DMS sampler2DMS
#define depth2DMSArray sampler2DMSArray
#define depthCube samplerCube
#define depthCubeArray samplerCubeArray
#define depth2DArrayShadow sampler2DArrayShadow

/* Backend Functions. */
#define select(A, B, mask) mix(A, B, mask)

bool is_zero(vec2 A)
{
  return all(equal(A, vec2(0.0)));
}

bool is_zero(vec3 A)
{
  return all(equal(A, vec3(0.0)));
}

bool is_zero(vec4 A)
{
  return all(equal(A, vec4(0.0)));
}
#define GPU_SHADER
#define GPU_INTEL
#define OS_UNIX
#define GPU_OPENGL
#define GPU_FRAGMENT_SHADER
#define USE_GPU_SHADER_CREATE_INFO

/* Pass Resources. */

/* Batch Resources. */

/* Push Constants. */
uniform mat4 ModelViewProjectionMatrix;
uniform vec4 parameters[4];
uniform float alpha;


/* Interfaces. */
in gpu_widget_shadow_iface{
  smooth float shadowFalloff;
};
layout(depth_any) out float gl_FragDepth;

/* Outputs. */
layout(location = 0) out vec4 fragColor;


void main()
{
  fragColor = vec4(0.0);
  /* Manual curve fit of the falloff curve of previous drawing method. */
  fragColor.a = alpha * (shadowFalloff * shadowFalloff * 0.722 + shadowFalloff * 0.277);
}

